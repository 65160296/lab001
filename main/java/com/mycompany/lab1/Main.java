import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        XO game = new XO();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to XO!");
        System.out.println("Player 1: X");
        System.out.println("Player 2: O");
        System.out.println("Let's start the game!\n");

        while (!game.checkWin() && !game.BoardFull()) {
            game.printBoard();
            int position;
            do{
                System.out.print("Player " + game.getCurrentPlayer() + ", enter your move (1-9): ");
                position = scanner.nextInt();
            } while (game.makeXO(position));

            if (game.checkWin()) {
                System.out.println("Player " + game.getCurrentPlayer() + " wins!");
            } else if (game.BoardFull()) {
                System.out.println("It's a draw!");
            }

            game.switchPlayer();
        }
        game.printBoard();
        scanner.close();
    }
}